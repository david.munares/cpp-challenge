/************************************************************/
/* Includes                                                 */
/************************************************************/

#include <iostream>

/************************************************************/
/* Main                                                     */
/************************************************************/

#define RACE_LENGTH 10
#define CAR_ID_1 1
#define CAR_ID_2 2

#define CAR_SPEED_1 250
#define CAR_SPEED_2 150

static const int RACE_TRACK[RACE_LENGTH]={1,1,1,1,1,1,1,1,1,2};

int main() {

    return EXIT_SUCCESS;
}
