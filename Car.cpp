/************************************************************/
/* Includes                                                 */
/************************************************************/

#include "Car.hpp"

/************************************************************/
/* Class                                                    */
/************************************************************/

Car::Car(uint16_t id , uint16_t speed, const int *race_track) :
        m_startThread(false),
        m_thread(),
        m_lock(),
        m_speed(speed),
        m_carId(id),
        m_position(0)
{
    for(uint8_t i=0 ; i<RACE_LENGTH  i++) m_track[i] = race_track[i];
}

//ToDo - modify
Car::~Car() {

}

//ToDo - modify
void Car::Thread() {

    while(m_startThread){



    }
}

//ToDo - modify
void Car::start() {

}

//ToDo - modify
void Car::registerCallback(const IVehicle::vehicleCallback &callback) {

}

//ToDo - modify
void Car::notify(const IVehicle::VehicleStatus &status, const uint16_t carId) {

}

IVehicle::VehicleStatus Car::move() {

    if(1 == get_trackVal(get_position())) set_position(get_position()+1);
    if(2 == get_trackVal(get_position())) return VehicleStatus::FINISHED;

    return VehicleStatus::RUNNING;
}

uint16_t Car::get_speed() const{
    return m_speed;
}

uint16_t Car::get_carId() const {
    return m_carId;
}

int Car::get_position() const {
    return m_position;
}

int Car::get_trackVal(int position) {
    return m_track[position];
}

void Car::set_position(int position) {
    m_position = position;
}