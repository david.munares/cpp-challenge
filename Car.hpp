#ifndef CPP_CHALLENGE_CAR_H
#define CPP_CHALLENGE_CAR_H

/************************************************************/
/* Includes                                                 */
/************************************************************/

#include "IVehicle.hpp"
#include <list>
#include <mutex>
#include <unistd.h>
#include <thread>

#define RACE_LENGTH 10

/************************************************************/
/* Class                                                    */
/************************************************************/

class Car
{
public:
    explicit Car(uint16_t id , uint16_t speed, const int *race_track);
    ~Car();

    void start();
    void registerCallback(const IVehicle::vehicleCallback& callback);

private:

    void Thread();
    VehicleStatus move();
    void notify(const IVehicle::VehicleStatus& status , const uint16_t carId);

    uint16_t get_speed() const;
    uint16_t get_carId() const;
    int get_position() const;
    int get_trackVal(int position);

    void set_position(int position);

    bool m_startThread;
    std::thread  m_thread;
    std::list<IVehicle::vehicleCallback> m_carCallback;
    std::mutex m_lock;

    uint16_t m_speed;
    uint16_t m_carId;
    int m_position;
    int  m_track[RACE_LENGTH];
};


#endif //CPP_CHALLENGE_CAR_H
