# Car race in C++

## Problem
The program goal is to simulate a race between 2 cars and print who's the winner once the race is finished.
In order to do so, you will modify the Car class methods and instantiate 2 cars in the main.

Here's the programming principles you need to know to accomplish this problem :
- Thread / mutex
- Callback
- Inheritance

### Question 1
Modify the Car class methods :
  - Thread() : Thread method that will notify the callback whenever the car has finished looping through the race track.
  - start() : Start the thread.
  - registerCallback() : Register a callback.
  - notify() : Notify callback parameters changes.
  - move() : Loop through the race track and returns the vehicle status.

### Question 2
Create 2 Car objects with the given parameters.

### Question 3
Register a callback for every object that will print the winner id and status.

### Nota Bene
- Don't hesitate to use valgrind to ensure your code is thread safe.
- Don't hesitate to make changes to the code provided if you see any possible improvements. 
- If both cars arrive at the same time, ensure that only 1 callback will be executed.

