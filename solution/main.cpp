/************************************************************/
/* Includes                                                 */
/************************************************************/

#include <iostream>
#include "Car.hpp"

using namespace std;
/************************************************************/
/* Main                                                     */
/************************************************************/

#define CAR_ID_1 1
#define CAR_ID_2 2

#define CAR_SPEED_1 250
#define CAR_SPEED_2 150

static const int RACE_TRACK[RACE_LENGTH]={1,1,1,1,1,1,1,1,1,2};

int main() {
    std::mutex m_lock;
    Car* car1 = new Car(CAR_ID_1,CAR_SPEED_1,RACE_TRACK);
    Car* car2 = new Car(CAR_ID_2,CAR_SPEED_2,RACE_TRACK);
    bool winner = false;

    car1->registerCallback([&](const IVehicle::VehicleStatus & status,const uint16_t id){
        std::unique_lock<std::mutex> lock(m_lock);
        if(!winner){
            cout<<"Car#"<<id;
            if(IVehicle::FINISHED == status) cout<<" FINISHED\n";
            winner = true;
        }
    });
    car2->registerCallback([&](const IVehicle::VehicleStatus & status,const uint16_t id){
        std::unique_lock<std::mutex> lock(m_lock);
        if(!winner){
            cout<<"Car#"<<id;
            if(IVehicle::FINISHED == status) cout<<" FINISHED\n";
            winner = true;
        }
    });

    car1->start();
    car2->start();
    while (!winner);
    // Possible improvement : Add SIGNAL handling to terminate program
    delete car1;
    delete car2;

    return EXIT_SUCCESS;
}
