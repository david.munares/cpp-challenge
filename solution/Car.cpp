/************************************************************/
/* Includes                                                 */
/************************************************************/

#include "Car.hpp"

/************************************************************/
/* Class                                                    */
/************************************************************/

// Possible improvement : std::copy() for the array
Car::Car(uint16_t id , uint16_t speed, const int *race_track) :
        m_startThread(false),
        m_thread(),
        m_lock(),
        m_speed(speed),
        m_carId(id),
        m_position(0)
{
    for(uint8_t i=0 ; i<RACE_LENGTH ; i++) m_track[i] = race_track[i];
}

Car::~Car() {
    m_startThread = false;

    if (m_thread.joinable()) {
        m_thread.join();
    }
}

// Possible improvement : Remove m_startThread to m_state
//                        While RUNNING move()
void Car::Thread() {

    while(m_startThread){

        m_lock.lock();
        if(VehicleStatus::FINISHED == move()){
            notify(VehicleStatus::FINISHED,get_carId());
        }
        m_lock.unlock();

        usleep(get_speed());
    }

}

IVehicle::VehicleStatus Car::move() {

    if(1 == get_trackVal(get_position())) set_position(get_position()+1);
    if(2 == get_trackVal(get_position())) return VehicleStatus::FINISHED;

    return VehicleStatus::RUNNING;
}

void Car::start() {
    if (!m_startThread) {
        m_thread = std::thread(&Car::Thread, this);
    }
    m_startThread = true;
}

void Car::registerCallback(const IVehicle::vehicleCallback &callback) {
    std::unique_lock<std::mutex> lock(m_lock);
    m_carCallback.push_back(callback);
}

void Car::notify(const IVehicle::VehicleStatus &status, const uint16_t carId) {
    if (!m_carCallback.empty()) {
        for (auto& cb : m_carCallback) {
            cb(status, carId);
        }
    }
}

uint16_t Car::get_speed() const{
    return m_speed;
}

uint16_t Car::get_carId() const {
    return m_carId;
}

int Car::get_position() const {
    return m_position;
}

void Car::set_position(int position) {
    m_position = position;
}

int Car::get_trackVal(int position) {
    return m_track[position];
}
