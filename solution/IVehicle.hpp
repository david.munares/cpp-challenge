#ifndef CPP_CHALLENGE_IVEHICLE_H
#define CPP_CHALLENGE_IVEHICLE_H

/************************************************************/
/* Includes                                                 */
/************************************************************/

#include <functional>

/************************************************************/
/* Class                                                    */
/************************************************************/

class IVehicle
{
public:
    enum VehicleStatus {
        FINISHED = 0,
        RUNNING = 1
    };

    typedef std::function<void(const VehicleStatus& , const uint16_t)> vehicleCallback;

    virtual ~IVehicle() = default;

    virtual VehicleStatus move() = 0;

};


#endif //CPP_CHALLENGE_IVEHICLE_H
